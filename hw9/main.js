"use strict"

const ul = document.querySelector('ul');

const arr1 = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];

const arr2 = ['hello', 'world', 'homework', '123'];

function CreateList (arr, list = document.body){
    arr.forEach(Element =>{
        const tag = document.createElement("li");
        tag.innerText = Element;
        list.appendChild(tag);
    })
}

CreateList (arr1, ul);
CreateList (arr2, ul);